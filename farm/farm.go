package farm

import (
	_boid "github.com/beerdeaap/boidsd/boid"
	"github.com/beerdeaap/boidsd/config"
	"github.com/beerdeaap/boidsd/particle"
	"github.com/beerdeaap/boidsd/static"
	"github.com/beerdeaap/boidsd/terrain"
	"github.com/beerdeaap/boidsd/vector"
	"math/rand"
	"sync"
	"time"
)

type Farm struct {
	Boids         []_boid.Boid
	SizeX         int
	SizeY         int
	Target        _boid.Boid
	Fences        []static.Line
	Trees         []static.Point
	Predators     []static.Point
	Configuration *config.Configuration
}

var WriteMu = &sync.Mutex{}

func (farm *Farm) InitializeRandom() {
	WriteMu.Lock()
	defer WriteMu.Unlock()
	// Create and seed the generator.
	// Typically a non-fixed seed should be used, such as time.Now().UnixNano().
	// Using a fixed seed will produce the same output on every run.
	r := rand.New(rand.NewSource(farm.Configuration.RandSource))
	// read starting positions for farm.Boids

	// init boids
	for i := 0; i < farm.Configuration.AmountOfBoids; i++ {
		position := vector.Point2d{XY: vector.NewXY(
			//divide by four to keep them a bit closer together to start with
			float64(r.Float64()+float64(farm.SizeX)/2),
			float64(r.Float64()+float64(farm.SizeY)/2),
		)}

		velocity := vector.Vector2d{XY: vector.XY{X: 0.4, Y: 0.4}}
		emitter := particle.ParticleEmitter{
			SpawnPosition:           position,
			SpawningRate:            1,
			SpawningTimeFrame:       time.Duration(90) * (time.Second / 1000),
			InitialParticleVelocity: vector.Vector2d{XY: vector.XY{X: 3, Y: 3}},
			Accumulator:             time.Duration(0) * time.Second,
			Ttl:                     time.Duration(500) * time.Second / 1000,
			InitialAlpha:            200,
			VelocityDegradation:     0.1,
		}

		farm.Boids = append(farm.Boids, *_boid.NewBoid(&position, &velocity, &emitter))
	}

	// init trees
	for i := 0; i < farm.Configuration.AmountOfTrees; i++ {
		position := vector.Point2d{XY: vector.NewXY(
			float64(r.Intn(farm.SizeX)),
			float64(r.Intn(farm.SizeY)),
		)}

		farm.Trees = append(farm.Trees, static.Point{Position: position})
	}

	// init fences FIXME: MAKE SURE TO MAKE IMAGE OF TERRAIN TO USE A BG IN
	// CLIENT
	terrain.CreateImage(farm.Configuration.MapFileName, farm.Configuration.MapSeed)
	contours, _ := terrain.ContoursFromImage(farm.Configuration.MapFileName)
	var idx int
	for _, contour := range contours {
		for idx, _ = range contour {
			if idx > 0 {
				a := vector.Point2d{XY: vector.NewXY(
					float64(contour[idx-1].X),
					float64(contour[idx-1].Y),
				)}

				b := vector.Point2d{XY: vector.NewXY(
					float64(contour[idx].X),
					float64(contour[idx].Y),
				)}

				farm.Fences = append(farm.Fences, static.Line{A: a, B: b})
			}
		}
		if idx > 1 {
			a := vector.Point2d{XY: vector.NewXY(
				float64(contour[0].X),
				float64(contour[0].Y),
			)}
			b := vector.Point2d{XY: vector.NewXY(
				float64(contour[idx].X),
				float64(contour[idx].Y),
			)}
			farm.Fences = append(farm.Fences, static.Line{A: a, B: b})
		}
	}

	// particles
}

func (farm *Farm) CentreOfMass(index_exclude int, boid _boid.Boid) vector.Point2d {
	total := vector.XY{X: 0, Y: 0}
	for index, other_boid := range farm.Boids {
		if other_boid.Position.XY.Distance(boid.Position.XY) < farm.Configuration.FlockDistance && index != index_exclude {
			total = total.Add(other_boid.Position.XY)
		}
	}
	return vector.Point2d{XY: total.Div(float64(len(farm.Boids) - 1))}
}

func (farm *Farm) MoveTowardsPerceivedCenter(index int, boid _boid.Boid) vector.Vector2d {
	perceivedCenter := farm.CentreOfMass(index, boid)
	if perceivedCenter.X > 0 && perceivedCenter.Y > 0 {
		return vector.Vector2d{XY: perceivedCenter.Sub(boid.Position.XY).Div(100)}
	}

	return vector.Vector2d{XY: vector.XY{X: 0, Y: 0}}
}

func (farm *Farm) KeepDistance(index_exclude int, boid _boid.Boid) vector.Vector2d {
	c := vector.Vector2d{XY: vector.XY{X: 0, Y: 0}}
	for index, other_boid := range farm.Boids {
		if index != index_exclude {
			distance := boid.Position.XY.Distance(other_boid.Position.XY)
			if distance > 0 && distance < farm.Configuration.MinDistance {
				c.XY = c.XY.Sub(other_boid.Position.XY.Sub(boid.Position.XY).Div(distance))
			}
		}
	}
	return c
}

func (farm *Farm) MatchVeloctiy(index_exclude int, boid _boid.Boid) vector.Vector2d {
	total_velocity := vector.Vector2d{XY: vector.XY{X: 0, Y: 0}}
	for index, other_boid := range farm.Boids {
		if index != index_exclude {
			total_velocity.XY = total_velocity.XY.Add(other_boid.Velocity.XY)
		}
	}

	average_velocity := total_velocity.XY.Div(float64(len(farm.Boids) - 1))
	return vector.Vector2d{XY: average_velocity.Sub(boid.Velocity.XY).Div(8.0)}
}

func (farm *Farm) TendToPlace(boid _boid.Boid) vector.Vector2d {
	place := farm.Target.Position.XY
	if place.X > 0 && place.Y > 0 {
		return vector.Vector2d{XY: place.Sub(boid.Position.XY).Div(100)}
	}
	return vector.Vector2d{XY: vector.XY{X: 0, Y: 0}}
}

func (farm *Farm) CapSpeed(boid *_boid.Boid) {
	speed := boid.Velocity.XY.Distance(vector.XY{X: 0, Y: 0})
	if speed > farm.Configuration.Vlim {
		boid.Velocity.XY = boid.Velocity.XY.Div(speed).Mult(farm.Configuration.Vlim)
	}
}

func (farm *Farm) InfinityBound(boid *_boid.Boid) {

	if boid.Position.XY.X > float64(farm.SizeX) {
		boid.Position.XY.X -= float64(farm.SizeX)
	} else if boid.Position.XY.X < 0 {
		boid.Position.XY.X += float64(farm.SizeX)
	}

	if boid.Position.XY.Y > float64(farm.SizeY) {
		boid.Position.XY.Y -= float64(farm.SizeY)
	} else if boid.Position.XY.Y < 0 {
		boid.Position.XY.Y += float64(farm.SizeY)
	}

}

func (farm *Farm) Wind() vector.Vector2d {
	return vector.Vector2d{XY: vector.XY{X: 0.0, Y: 0}}
}

func (farm *Farm) MoveAwayFromTrees(boid _boid.Boid) vector.Vector2d {
	newVector2d := vector.Vector2d{XY: vector.XY{X: 0, Y: 0}}
	for _, tree := range farm.Trees {

		if boid.Position.XY.Distance(tree.Position.XY) < (farm.Configuration.DistanceFromTree) {
			newVector2d.XY = newVector2d.XY.Sub(tree.Position.XY.Sub(boid.Position.XY)).Div(2)
		}
	}
	return newVector2d
}

func (farm *Farm) MoveAwayFromPredator(boid _boid.Boid) vector.Vector2d {
	newVector2d := vector.Vector2d{XY: vector.XY{X: 0, Y: 0}}
	for _, predator := range farm.Predators {
		if boid.Position.XY.Distance(predator.Position.XY) < (farm.Configuration.DistanceFromPredator) {
			newVector2d.XY = newVector2d.XY.Sub(predator.Position.XY.Sub(boid.Position.XY)).Div(2)
		}
	}
	return newVector2d
}

func (farm *Farm) MoveAwayFromFences(boid _boid.Boid) vector.Vector2d {
	newVector2d := vector.Vector2d{XY: vector.XY{X: 0, Y: 0}}
	for _, fence := range farm.Fences {
		if fence.DistanceFrom(boid.Position) < (farm.Configuration.DistanceFromFence) {
			point := static.Point{Position: boid.Position}
			p_slope := fence.GetPerpendicularSlope()
			p_y_intercept := point.GetYIntercept(p_slope)

			point_on_fence := fence.GetClosestPointOnLine(p_slope, p_y_intercept)
			// find point on fence closest to boid, steer away from there
			//newVector2d.XY = newVector2d.XY.Sub(tree.Position.XY.Sub(boid.Position.XY)).Div(2)
			newVector2d.XY = newVector2d.XY.Sub(point_on_fence.XY.Sub(boid.Position.XY))
		}
	}
	return newVector2d
}

func (farm *Farm) SlowDown(boid _boid.Boid) vector.Vector2d {
	return vector.Vector2d{XY: boid.Velocity.XY.Mult(-0.5)}
}

func (farm *Farm) CapSteer(v *vector.Vector2d) {

	if v.XY.X > farm.Configuration.MaxSteeringForce {
		v.XY.X = farm.Configuration.MaxSteeringForce
	}
	if v.XY.X < -farm.Configuration.MaxSteeringForce {
		v.XY.X = -farm.Configuration.MaxSteeringForce
	}

	if v.XY.Y > farm.Configuration.MaxSteeringForce {
		v.XY.Y = farm.Configuration.MaxSteeringForce
	}
	if v.XY.Y < -farm.Configuration.MaxSteeringForce {
		v.XY.Y = -farm.Configuration.MaxSteeringForce
	}
}

func (farm *Farm) Update(dt time.Duration) {

	// called on every click. Runs all rules and update functions
	newBoids := make([]_boid.Boid, len(farm.Boids))
	for index, boid := range farm.Boids {
		WriteMu.Lock()
		rules := []vector.XY{
			farm.MoveAwayFromTrees(boid).Mult(farm.Configuration.MoveAwayFromTreesMultiplier),
			farm.MoveAwayFromFences(boid).Mult(farm.Configuration.MoveAwayFromFencesMultiplier),
			farm.MoveAwayFromPredator(boid).Mult(farm.Configuration.MoveAwayFromPredatorsMultiplier),
			farm.MoveTowardsPerceivedCenter(index, boid).Mult(farm.Configuration.MoveTowardsPerceivedCenterMultiplier),
			farm.KeepDistance(index, boid).Mult(farm.Configuration.KeepDistanceMultiplier),
			farm.MatchVeloctiy(index, boid).Mult(farm.Configuration.MatchVeloctiyMultiplier),
			farm.TendToPlace(boid).Mult(farm.Configuration.TendToPlaceMultiplier),
			//farm.BoundPosition(boid),
			//farm.SlowDown(boid).Mult(0.5),
		}

		desiredVelocity := boid.Velocity.XY
		for _, rule := range rules {
			desiredVelocity = desiredVelocity.Add(rule)
		}

		// make sure we steer smoothly
		steerVelocity := vector.Vector2d{XY: desiredVelocity.Sub(boid.Velocity.XY)}
		farm.CapSteer(&steerVelocity)
		boid.Velocity.XY = boid.Velocity.XY.Add(steerVelocity.XY)
		farm.CapSpeed(&boid)

		boid.Position.XY = boid.Position.XY.Add(boid.Velocity.XY)
		boid.Emitter.Update(dt)
		boid.Emitter.SpawnPosition = boid.Position
		farm.InfinityBound(&boid)
		newBoids[index] = *_boid.NewBoid(&boid.Position, &boid.Velocity, &boid.Emitter)
		WriteMu.Unlock()
	}
	WriteMu.Lock()
	farm.Boids = newBoids
	WriteMu.Unlock()
}
