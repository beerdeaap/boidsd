// https://www.redblobgames.com/maps/terrain-from-noise/
package terrain

import (
	simplex "github.com/ojrac/opensimplex-go"
	"gocv.io/x/gocv"
	"image"
	"image/color"
	"image/png"
	"os"
)

type Terrain [][]float64

var (
	RED         = color.RGBA{158, 57, 26, 255}
	BEIGE       = color.RGBA{196, 172, 124, 255}
	GREEN       = color.RGBA{94, 106, 88, 255}
	GREENDARK   = color.RGBA{53, 69, 54, 255}
	GREENDARKER = color.RGBA{45, 67, 58, 255}
)

func InitializeTerrain(seed int64, sizeX int, sizeY int) (terrain Terrain) {
	n := simplex.NewWithSeed(seed)

	terrain = make(Terrain, sizeX)

	for x := 0; x < sizeX; x++ {
		terrain[x] = make([]float64, sizeY)
		for y := 0; y < sizeY; y++ {
			nx := float64(x)/float64(sizeY) - 0.5
			ny := float64(y)/float64(sizeY) - 0.5
			// add noise on different frequencies
			terrain[x][y] = n.Eval2(float64(nx)*3.1, float64(ny)*3.1)
			terrain[x][y] = (terrain[x][y] / 2) + 0.5 // Rescale from -1.0:+1.0 to 0.0:1.0
		}
	}

	return
}

// use config
func CreateImage(filename string, seed int64) {

	f, _ := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE, 0600)
	defer f.Close()

	var clr color.RGBA

	// FIXME: use size from settings
	terrain := InitializeTerrain(seed, 1600, 900)
	im := image.NewRGBA(image.Rect(0, 0, 1600, 900))

	// Only use two colors so we can draw nice polys
	for x := range terrain {
		for y := range terrain[x] {
			v := terrain[x][y]
			switch {
			//case v > 0.8:
			//	clr = RED
			case v > 0.6:
				clr = BEIGE
			//case v > 0.4:
			//	clr = GREEN
			//case v > 0.2:
			//	clr = GREENDARK
			default:
				clr = GREENDARKER
			}
			im.Set(x, y, clr)
		}
	}

	png.Encode(f, im)
}

func ContoursFromImage(filename_in string) ([][]image.Point, gocv.Mat) {
	// convert image to grayscale and find edges using opencv
	// convert many points of contours to polys

	// FIXME: make some output options optional (normaly we should just return
	// a list of coords so we can use them to draw lines)
	thresh := gocv.NewMat()
	imGray := gocv.NewMat()
	im := gocv.IMRead(filename_in, gocv.IMReadColor)
	gocv.CvtColor(im, &imGray, gocv.ColorBGRAToGray)
	gocv.Threshold(imGray, &thresh, 127, 255, gocv.ThresholdBinary)
	contours := gocv.FindContours(thresh, gocv.RetrievalTree, gocv.ChainApproxTC89KCOS)

	// make contours into lines
	for idx, contour := range contours {
		// FIXME: make configurable
		epsilon := 0.009 * gocv.ArcLength(contour, true)
		contours[idx] = gocv.ApproxPolyDP(contour, epsilon, true)
	}

	return contours, im
}

func ContoursFromImageToFile(filename_in, filename_out string) {
	contours, im := ContoursFromImage(filename_in)
	gocv.DrawContours(&im, contours, -1, color.RGBA{50, 255, 50, 100}, 5)
	gocv.IMWrite(filename_out, im)
}
