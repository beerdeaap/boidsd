package terrain_test

import (
	"github.com/beerdeaap/boidsd/terrain"
	"testing"
)

func TestCreateImage(t *testing.T) {
	terrain.CreateImage("out.png", 2)
	terrain.ContoursFromImageToFile("out.png", "out-contours.png")
}
