package boid

import (
	"github.com/beerdeaap/boidsd/particle"
	"github.com/beerdeaap/boidsd/vector"
)

type Boid struct {
	Position vector.Point2d
	Velocity vector.Vector2d
	Emitter  particle.ParticleEmitter
}

func NewBoid(position *vector.Point2d, velocity *vector.Vector2d, emitter *particle.ParticleEmitter) *Boid {
	return &Boid{Position: *position, Velocity: *velocity, Emitter: *emitter}
}
