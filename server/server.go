package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	_farm "github.com/beerdeaap/boidsd/farm"
	"github.com/beerdeaap/boidsd/static"
	"github.com/beerdeaap/boidsd/vector"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// FIXME: add some logging
func Server(farm *_farm.Farm) {

	http.HandleFunc("/boids", func(w http.ResponseWriter, r *http.Request) {
		conn, _ := upgrader.Upgrade(w, r, nil)
		for {
			msgType, msg, err := conn.ReadMessage()
			if err != nil {
				return
			}

			if string(msg) == "GET" {
				_farm.WriteMu.Lock()
				jsonData, err := json.Marshal(farm)
				_farm.WriteMu.Unlock()
				if err != nil {
					fmt.Println("Error:", jsonData)
					return
				}

				if err = conn.WriteMessage(msgType, jsonData); err != nil {
					return
				}
			}

		}
	})

	http.HandleFunc("/set_target", func(w http.ResponseWriter, r *http.Request) {

		conn, _ := upgrader.Upgrade(w, r, nil)
		for {
			_, msg, err := conn.ReadMessage()
			if err != nil {
				return
			}

			xy := strings.Split(string(msg), ",")
			x, _ := strconv.ParseInt(xy[0], 10, 0)
			y, _ := strconv.ParseInt(xy[1], 10, 0)

			_farm.WriteMu.Lock()
			farm.Target.Position.X = float64(x)
			farm.Target.Position.Y = float64(y)
			_farm.WriteMu.Unlock()
			return
		}
	})

	http.HandleFunc("/add_predator", func(w http.ResponseWriter, r *http.Request) {
		conn, _ := upgrader.Upgrade(w, r, nil)
		for {
			_, msg, err := conn.ReadMessage()
			if err != nil {
				return
			}

			xy := strings.Split(string(msg), ",")
			x, _ := strconv.ParseFloat(xy[0], 10)
			y, _ := strconv.ParseFloat(xy[1], 10)

			point := static.Point{Position: vector.Point2d{XY: vector.XY{X: x, Y: y}}}

			if len(farm.Predators) < farm.Configuration.MaxPredators {
				_farm.WriteMu.Lock()
				farm.Predators = append(farm.Predators, point)
				_farm.WriteMu.Unlock()
			}
			return
		}
	})

	// FIXME: make configurable
	fmt.Printf("Listening on port %v\n", farm.Configuration.Port)
	http.ListenAndServe(fmt.Sprintf(":%v", farm.Configuration.Port), nil)
}
