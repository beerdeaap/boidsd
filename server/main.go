package main

import (
	config "github.com/beerdeaap/boidsd/config"
	_farm "github.com/beerdeaap/boidsd/farm"
	_ "math/rand"
	"time"
)

var Configuration config.Configuration

func main() {
	config.LoadConfig(&Configuration)
	farm := _farm.Farm{
		SizeX:         Configuration.ScreenSizeX,
		SizeY:         Configuration.ScreenSizeY,
		Configuration: &Configuration,
	}
	farm.InitializeRandom()
	// main loop
	// FIXME: send deltatime to update
	timer := time.Tick(time.Duration(Configuration.Tick) * time.Millisecond)
	go func() {
		last := time.Now()
		for now := range timer {
			farm.Update(time.Since(last))
			last = now
		}
	}()
	// FIXME: make sure we fail if we can't listen on port
	Server(&farm)
}
