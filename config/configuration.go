package config

import (
	"fmt"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"sync"
)

type Configuration struct {
	ScreenSizeX int
	ScreenSizeY int
	Tick        int

	Port int

	AmountOfBoids  int
	AmountOfTrees  int
	AmountOfFences int

	MaxPredators  int
	RandSource    int64
	FlockDistance float64 //100000  does not work as expected. Keep high

	MinDistance          float64
	Vlim                 float64
	MaxSteeringForce     float64
	DistanceFromTree     float64
	DistanceFromFence    float64
	DistanceFromPredator float64

	MoveTowardsPerceivedCenterMultiplier float64
	KeepDistanceMultiplier               float64
	MatchVeloctiyMultiplier              float64
	TendToPlaceMultiplier                float64
	MoveAwayFromTreesMultiplier          float64
	MoveAwayFromFencesMultiplier         float64
	MoveAwayFromPredatorsMultiplier      float64

	MapSeed     int64
	MapFileName string
}

var configMu sync.Mutex

func LoadConfig(configuration *Configuration) {
	configMu.Lock()
	defer configMu.Unlock()
	viper.SetConfigName("config")
	viper.AddConfigPath("../")

	err := viper.ReadInConfig()
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error reading config file: %s \n", err))
	}

	err = viper.Unmarshal(configuration)
	if err != nil { // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error unmarshallling config file: %s \n", err))
	}

	// fmt.Printf("%+v\n", *configuration)

	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Reloading configuration", e.Name)
		configMu.Lock()
		defer configMu.Unlock()
		err := viper.Unmarshal(configuration)
		if err != nil { // Handle errors reading the config file
			panic(fmt.Errorf("Fatal error unmarshallling config file: %s \n", err))
		}

		// fmt.Printf("%+v\n", *configuration)

	})

}
