package main

import (
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"image/color"
)

type XY struct {
	X int
	Y int
}

type Container interface {
	AppendChild(container *Container)
	GetChildren() []*Container
}

type Drawable interface {
	Draw(window *pixelgl.Window)
}

type Frame struct {
	Title string
	Size  XY

	BorderColor color.RGBA

	BorderThickness int

	Position XY

	Children []*Container
	// Packmode
}

func (f *Frame) AppendChild(container *Container) {
	f.Children = append(f.Children, container)
}

func (f *Frame) GetChildren() []*Container {
	return f.Children
}

func (f *Frame) Draw(window *pixelgl.Window) {
	imdraw.
}
