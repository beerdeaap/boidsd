package vector

import (
	"fmt"
	"math"
)

type XY struct {
	X, Y float64
}

type Vector2d struct {
	XY
}

type Point2d struct {
	XY
}

func NewXY(x float64, y float64) XY {
	return XY{X: x, Y: y}
}

func (v XY) Add(v2 XY) XY {
	return XY{X: v.X + v2.X, Y: v.Y + v2.Y}
}

func (v XY) Sub(v2 XY) XY {
	return XY{X: v.X - v2.X, Y: v.Y - v2.Y}
}

func (v XY) Div(d float64) XY {
	return XY{X: v.X / d, Y: v.Y / d}
}

func (v XY) Mult(m float64) XY {
	return XY{X: v.X * m, Y: v.Y * m}
}

func (v XY) String() string {
	return fmt.Sprintf("[%0.5f, %0.5f]", v.X, v.Y)
}

func (v XY) Distance(v2 XY) float64 {
	// not sure if this really works as expected
	return math.Sqrt(math.Pow(v.X-v2.X, 2) + math.Pow(v.Y-v2.Y, 2))
}

func (v XY) Dot(v2 XY) float64 {
	return (v.X * v2.X) + (v.Y * v2.Y)
}

func (v XY) Rotation() float64 {
	return math.Atan2(v.Y, v.X)
}

func (v XY) Magnitude() float64 {
	return math.Sqrt(v.Dot(v))
}

func (v XY) Normalize() XY {
	return XY{X: v.X / v.Magnitude(), Y: v.Y / v.Magnitude()}
}

func (v XY) Angle(v2 XY) float64 {
	return math.Acos(v.Dot(v2) / (v.Magnitude() * v2.Magnitude()))
}
