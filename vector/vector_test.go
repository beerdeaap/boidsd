package vector_test

import (
	"fmt"
	"github.com/beerdeaap/boidsd/vector"
	"math"
	"testing"
)

func TestXYAdd(t *testing.T) {
	xy := vector.XY{X: 10, Y: 20}
	newXY := xy.Add(vector.XY{X: 1, Y: 2})

	expected := 11.
	if newXY.X != expected {
		t.Errorf("Expected newXY.X to be %v. Got %v", expected, newXY.X)
	}

	expected = 22.
	if newXY.Y != expected {
		t.Errorf("Expected newXY.Y to be %v. Got %v", expected, newXY.Y)
	}
}

func TestXYSub(t *testing.T) {

	xy := vector.XY{X: 3, Y: 40}

	newXY := xy.Sub(vector.XY{X: 2, Y: 20})

	expected := 1.
	if newXY.X != expected {
		t.Errorf("Expected newXY.X to be %v. Got %v", expected, newXY.X)
	}

	expected = 20.
	if newXY.Y != expected {
		t.Errorf("Expected newXY.Y to be %v. Got %v", expected, newXY.Y)
	}
}

func TestXYDiv(t *testing.T) {

	xy := vector.XY{X: 6, Y: 40}

	newXY := xy.Div(2)
	expected := 3.0
	if newXY.X != expected {
		t.Errorf("Expected newXY.X to be %v. Got %v", expected, newXY.X)
	}
	expected = 20.
	if newXY.Y != expected {
		t.Errorf("Expected newXY.Y to be %v. Got %v", expected, newXY.Y)
	}
}

func TestXYMult(t *testing.T) {

	xy := vector.XY{X: 6, Y: 40}

	newXY := xy.Mult(2)

	expected := 12.
	if newXY.X != expected {
		t.Errorf("Expected newXY.X to be %v. Got %v", expected, newXY.X)
	}

	expected = 80.
	if newXY.Y != expected {
		t.Errorf("Expected newXY.X to be %v. Got %v", expected, newXY.Y)
	}
}

func TestXYDot(t *testing.T) {

	xy := vector.XY{X: 6, Y: 40}
	product := xy.Dot(vector.XY{X: 3, Y: 0.1})
	expected := 22.
	if product != expected {
		t.Errorf("Expected product to be %v. got %v", expected, product)
	}
}

func TestXYDistance(t *testing.T) {

	tables := []struct {
		v1       vector.XY
		v2       vector.XY
		expected float64
	}{
		{vector.XY{X: -5.2, Y: 3.8}, vector.XY{X: 8.7, Y: -4.1}, 15.988121},
		{vector.XY{X: 4009.9, Y: 3.8}, vector.XY{X: 4009, Y: 3.3}, 1.029563},
		{vector.XY{X: 1.01, Y: 1.01}, vector.XY{X: 1.01, Y: 1.01}, 0.},
	}

	for _, table := range tables {
		distance := table.v1.Distance(table.v2)
		if math.Abs(distance-table.expected) > 0.000001 {
			t.Errorf("Expected distance to be %v. Got %v", table.expected, distance)
		}
	}
}

func TestXYMagnitude(t *testing.T) {

	xy := vector.XY{X: 3, Y: 2}
	magnitude := xy.Magnitude()

	expected := 3.605551
	if math.Abs(magnitude-expected) > 0.000001 {
		t.Errorf("Exepected magnitude to be %v. Got %v", expected, magnitude)
	}
}

func TestString(t *testing.T) {

	xy := vector.XY{X: 4.799, Y: 5.897}
	expected := "[4.79900, 5.89700]"
	result := fmt.Sprintf("%v", xy)

	if result != expected {
		t.Errorf("Expected xy to be %v. Got %v", expected, result)
	}

}

func TestRotation(t *testing.T) {

	xy := vector.NewXY(4.799, 5.897)
	rotation := xy.Rotation()
	expected := 0.68310129877

	if math.Abs(rotation-expected) > 0.000001 {

		t.Errorf("Expected rotation to be %v. Got %v", expected, rotation)
	}
}

func TestNormalize(t *testing.T) {
	tables := []struct {
		v        vector.XY
		expected vector.XY
	}{
		{vector.XY{X: -5.2, Y: 3.8}, vector.XY{X: -0.80739, Y: 0.59002}},
		{vector.XY{X: 4009.9, Y: 3.8}, vector.XY{X: 1.0, Y: 0.00095}},
		{vector.XY{X: 1.01, Y: 1.01}, vector.XY{X: 0.70711, Y: 0.70711}},
	}

	for _, table := range tables {
		result := table.v.Normalize()
		if result.Distance(table.expected) > 0.00001 {
			t.Errorf("Expected normalized vector to be %v. Got %v", table.expected, result)
		}
	}

}

func TestAngle(t *testing.T) {

	tables := []struct {
		v        vector.XY
		v2       vector.XY
		expected float64
	}{
		{vector.NewXY(1.5, 3.6), vector.NewXY(1.5, 3.6), 1.1538066339048396},
		{vector.NewXY(1.5, 999), vector.NewXY(-1.5, 3.6), 0.3962926200728827},
	}

	for _, table := range tables {
		result := table.v.Angle(table.v2)
		if math.Abs(result-table.expected) > 0.000001 {
			t.Errorf("Expected angle to be %v. Got %v", table.expected, result)
		}
	}
}
