package static

import (
	"github.com/beerdeaap/boidsd/vector"
	"github.com/twpayne/go-geom"
	"github.com/twpayne/go-geom/xy"
)

type StaticGeometry interface {
	DistanceFrom(point vector.Point2d) float64
}

type Point struct {
	Position vector.Point2d
}

func (p Point) DistanceFrom(p2 vector.Point2d) float64 {
	return p.Position.XY.Distance(p2.XY)
}

func (point Point) GetYIntercept(slope float64) float64 {
	// Get Y intercept based on slope and the point, can be used later to
	// determine interction with perpendicular line
	return -((slope * point.Position.X) - point.Position.Y)
}

type Line struct {
	A vector.Point2d
	B vector.Point2d
	// FIXME: OPTIMIZE BY ONLY calculating slope at start
}

func (l Line) DistanceFrom(p vector.Point2d) float64 {
	_p := geom.Coord{p.X, p.Y}
	lineStart := geom.Coord{l.A.X, l.A.Y}
	lineEnd := geom.Coord{l.B.X, l.B.Y}
	distance := xy.DistanceFromPointToLine(_p, lineStart, lineEnd)

	return distance
}

func (l Line) GetSlope() float64 {
	return ((l.A.Y - l.B.Y) / (l.A.X - l.B.X))
}

func (l Line) GetPerpendicularSlope() float64 {
	// Get slope of line perpendicular to l we can use this to get
	return -1 / l.GetSlope()
}

func (l Line) GetYIntercept() float64 {
	point := Point{Position: l.A}
	return point.GetYIntercept(l.GetSlope())
}

func (l Line) GetClosestPointOnLine(other_slope float64, other_y_intercept float64) vector.Point2d {
	// Given a slope and Y intercept of another line (usually a perpendicular
	// line) we calculate at what point these two lines cross

	x := ((other_y_intercept) - l.GetYIntercept()) / (l.GetSlope() + -(other_slope))
	y := (l.GetSlope() * x) + l.GetYIntercept()
	return vector.Point2d{XY: vector.XY{X: x, Y: y}}
}

/*
m = slope
b = y intercept

m = (y2 - y1) / (x2 - x1)
b = (slope * point.X) - point.Y

intersection_x = (b2 - b1) / (slope1 + slope2)
intersection_y =
*/
