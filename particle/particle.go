package particle

import (
	"github.com/beerdeaap/boidsd/vector"
	// "math/rand"
	"time"
)

// FIXME: handle NaN
type ParticleEmitter struct {
	SpawnPosition           vector.Point2d
	SpawningRate            int
	SpawningTimeFrame       time.Duration
	InitialParticleVelocity vector.Vector2d
	InitialAlpha            float64
	Accumulator             time.Duration
	Ttl                     time.Duration
	VelocityDegradation     float64

	//FIXME: Add color
	Particles []Particle
}

type Particle struct {
	Position vector.Point2d
	Velocity vector.Vector2d
	Alpha    float64

	Ttl time.Duration
}

func (pe *ParticleEmitter) Update(dt time.Duration) {

	// remove particles that have no ttl left
	var particles []Particle
	for _, particle := range pe.Particles {
		if particle.Ttl > 0 {
			particles = append(particles, particle)
		}
	}
	pe.Particles = particles

	for i, particle := range pe.Particles {
		// Update existing particles
		pe.Particles[i].Ttl -= dt

		// update position by vector times delta time
		pe.Particles[i].Position.XY.X += particle.Velocity.XY.X
		pe.Particles[i].Position.XY.Y += particle.Velocity.XY.Y

		//degrade velocity
		// pe.Particles[i].Velocity.XY.X -= (pe.InitialParticleVelocity.XY.X * pe.VelocityDegradation)
		// pe.Particles[i].Velocity.XY.Y -= (pe.InitialParticleVelocity.XY.Y * pe.VelocityDegradation)

		pe.Particles[i].Alpha -= (pe.InitialAlpha / pe.Ttl.Seconds() / 3) * (pe.Particles[i].Ttl.Seconds() / 3)

		// upate vector by random crap
		// update color + alpha
	}

	// Add new particles if needed
	pe.Accumulator += dt
	if pe.Accumulator > pe.SpawningTimeFrame {
		for i := 0; i < pe.SpawningRate; i++ {
			// vel := vector.Vector2d{XY: vector.NewXY((rand.Float64()*-2)+1, (rand.Float64()*-2)+1)}
			vel := vector.Vector2d{XY: vector.NewXY(0, 0)}
			pe.Particles = append(pe.Particles, Particle{
				Position: pe.SpawnPosition,
				Velocity: vel,
				Ttl:      pe.Ttl,
				Alpha:    pe.InitialAlpha,
			})
		}
		pe.Accumulator = 0
	}
}
