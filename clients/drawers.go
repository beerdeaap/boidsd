package main

import (
	"github.com/beerdeaap/boidsd/boid"
	"github.com/beerdeaap/boidsd/farm"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/imdraw"
	"github.com/faiface/pixel/pixelgl"
	"golang.org/x/image/colornames"
)

type Drawable interface {
	Draw(win *pixelgl.Window)
}

type FarmDrawer struct {
	Farm *farm.Farm
}

type BoidDrawer struct {
	Boid *boid.Boid
}

func (d FarmDrawer) Draw(win *pixelgl.Window, sprites Sprites) {
	// background
	//sprites.background.Draw(win, pixel.IM.Moved(win.Bounds().Center()))
	// draw boids
	sprites.boidsBatch.Clear()
	imd := imdraw.New(nil)
	for i, boid := range d.Farm.Boids {
		mat := pixel.IM
		mat = mat.Moved(pixel.V(boid.Position.X, boid.Position.Y))
		mat = mat.Rotated(
			pixel.V(boid.Position.X, boid.Position.Y),
			pixel.V(boid.Velocity.X, boid.Velocity.Y).Angle()-0.785) // 45 degrees == ~0.785 radians)
		mat = mat.Scaled(
			pixel.V(boid.Position.X, boid.Position.Y), 0.5)
		sprites.boids[i].Draw(sprites.boidsBatch, mat)

		for _, particle := range boid.Emitter.Particles {
			imd.Color = pixel.RGB(1, 1, 0).Mul(pixel.Alpha(particle.Alpha / 255))
			imd.Push(pixel.V(particle.Position.X, particle.Position.Y))
			imd.Circle(1, 0)
		}

	}

	sprites.boidsBatch.Draw(win)
	// draw target
	mat := pixel.IM
	mat = mat.Moved(pixel.V(d.Farm.Target.Position.X, d.Farm.Target.Position.Y))
	sprites.target.Draw(win, mat)

	// draw fences
	// FIXME: make optional
	for _, fence := range d.Farm.Fences {
		imd.Color = colornames.White
		imd.EndShape = imdraw.RoundEndShape
		imd.Push(pixel.V(fence.A.X, fence.A.Y), pixel.V(fence.B.X, fence.B.Y))
		imd.Line(3)
	}

	//draw predators
	for _, predator := range d.Farm.Predators {
		imd.Color = pixel.RGB(1, 0, 1).Mul(pixel.Alpha(.25))

		imd.Push(pixel.V(predator.Position.X, predator.Position.Y))
		imd.Circle(50, 0)
	}

	//draw particles

	imd.Draw(win)
}
