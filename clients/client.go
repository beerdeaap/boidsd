package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/beerdeaap/boidsd/farm"
	"github.com/faiface/pixel"
	"github.com/faiface/pixel/pixelgl"
	"github.com/gorilla/websocket"
	"golang.org/x/image/colornames"
	"log"
	"net/url"
	"os"
	"os/signal"
	"time"
)

func closeConnection(client *websocket.Conn, done chan struct{}) {

	// Cleanly close the connection by sending a close message and then
	// waiting (with timeout) for the server to close the connection.
	err := client.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
	if err != nil {
		log.Println("write close:", err)
		return
	}

	select {
	case <-done:
	case <-time.After(time.Second):
	}

}

func initializeWindow() (win *pixelgl.Window) {
	cfg := pixelgl.WindowConfig{
		Title:  "Boid Client",
		Bounds: pixel.R(0, 0, 1600, 900),
		//VSync:  true,
	}

	win, err := pixelgl.NewWindow(cfg)
	if err != nil {
		panic(err)
	}

	win.SetSmooth(true)
	return
}

func run() {
	flag.Parse()

	// FIXME: move (part of) network crap to separate file
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)
	boidsUrl := url.URL{Scheme: "ws", Host: *addr, Path: "/boids"}

	log.Printf("connecting to %s", boidsUrl.String())

	client, _, err := websocket.DefaultDialer.Dial(boidsUrl.String(), nil)
	if err != nil {
		log.Fatal("dial:", err)
	}
	defer client.Close()

	done := make(chan struct{})
	drawer := FarmDrawer{}

	go func() {
		defer close(done)
		// FIXME: configurable
		ticker := time.Tick(33 * time.Millisecond)

		for _ = range ticker {
			client.WriteMessage(websocket.TextMessage, []byte("GET"))
			_, message, err := client.ReadMessage()
			if err != nil {
				log.Println("read:", err)
				return
			}

			json.Unmarshal(message, &drawer.Farm)
		}
	}()

	win := initializeWindow()
	sprites := NewSprites()

	for !win.Closed() {
		select {
		case <-done:
			return
		case <-interrupt:
			log.Println("interrupt")
			closeConnection(client, done)
			return
		default:
			win.Clear(colornames.Black)
			handleEvents(win, sprites)
			drawer.Draw(win, sprites)
			win.Update()
		}
	}

	log.Println("Window closed")
	closeConnection(client, done)
}

func handleEvents(win *pixelgl.Window, sprites Sprites) {

	if win.Pressed(pixelgl.MouseButtonLeft) {
		targetUrl := url.URL{Scheme: "ws", Host: *addr, Path: "/set_target"}

		log.Printf("connecting to %s", targetUrl.String())

		client, _, err := websocket.DefaultDialer.Dial(targetUrl.String(), nil)
		if err != nil {
			log.Fatal("dial:", err)
		}
		defer client.Close()
		pos := win.MousePosition()
		client.WriteMessage(websocket.TextMessage, []byte(fmt.Sprintf("%v,%v", pos.X, pos.Y)))
	}

	if win.JustPressed(pixelgl.MouseButtonRight) {
		targetUrl := url.URL{Scheme: "ws", Host: *addr, Path: "/add_predator"}

		log.Printf("connecting to %s", targetUrl.String())

		client, _, err := websocket.DefaultDialer.Dial(targetUrl.String(), nil)
		if err != nil {
			log.Fatal("dial:", err)
		}
		defer client.Close()
		pos := win.MousePosition()
		client.WriteMessage(websocket.TextMessage, []byte(fmt.Sprintf("%v,%v", pos.X, pos.Y)))
	}

}

var addr = flag.String("addr", "localhost:63333", "boid service address")
var f farm.Farm

func main() {
	pixelgl.Run(run)
}
