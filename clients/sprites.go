package main

import (
	"github.com/faiface/pixel"
	"image"
	_ "image/png"
	"os"
)

func loadPicture(path string) (pixel.Picture, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	img, _, err := image.Decode(file)
	if err != nil {
		return nil, err
	}
	return pixel.PictureDataFromImage(img), nil
}

type Sprites struct {
	boidsBatch *pixel.Batch
	boids      []pixel.Sprite
	target     pixel.Sprite
	background pixel.Sprite
}

func NewSprites() (s Sprites) {

	s = Sprites{}
	boidPic, err := loadPicture("boid.png")
	if err != nil {
		panic(err)
	}

	s.boidsBatch = pixel.NewBatch(&pixel.TrianglesData{}, boidPic)

	for i := 0; i < 1000; i++ {
		s.boids = append(s.boids, *pixel.NewSprite(boidPic, boidPic.Bounds()))
	}

	targetPic, err := loadPicture("boid_target.png")
	if err != nil {
		panic(err)
	}

	s.target = *pixel.NewSprite(targetPic, targetPic.Bounds())

	backgroundPicture, err := loadPicture("../server/terrain.png")
	s.background = *pixel.NewSprite(backgroundPicture, backgroundPicture.Bounds())
	return
}
